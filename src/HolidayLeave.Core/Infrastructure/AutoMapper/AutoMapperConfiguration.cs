﻿using AutoMapper;
using System;
using System.Linq;

namespace HolidayLeave.Core.Infrastructure.AutoMapper
{
    public class AutoMapperConfiguration
    {
        public IMapper Configure()
        {
            var profiles = AppDomain.CurrentDomain.GetAssemblies()
                .Where(x => x.FullName.Contains("Project"))
                .SelectMany(s => s.GetTypes())
                .Where(a => typeof(AutoMapperProfile).IsAssignableFrom(a));

            var mapperConfiguration = new MapperConfiguration(a => profiles.ForEach(a.AddProfile));

            return mapperConfiguration.CreateMapper();
        }
    }
}
