﻿using HolidayLeave.Data.Entities;
using System.Collections.Generic;

namespace HolidayLeave.Data
{
    public interface IData
    {
        void Add(HolidayLeaveEntity holidayLeaveEntity);

        List<HolidayLeaveEntity> GetHolidayLeaves();

        void Clear();
    }

    public class Data : IData
    {
        private static List<HolidayLeaveEntity> holidayLeaves;

        static Data()
        {
            holidayLeaves = new List<HolidayLeaveEntity>();
        }

        public void Add(HolidayLeaveEntity holidayLeaveEntity) =>
            holidayLeaves.Add(holidayLeaveEntity);

        public List<HolidayLeaveEntity> GetHolidayLeaves() =>
            holidayLeaves;

        public void Clear()
        {
            holidayLeaves
                .Clear();
        }
    }
}
