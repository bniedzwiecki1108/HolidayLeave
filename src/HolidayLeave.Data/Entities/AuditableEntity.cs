﻿using System;

namespace HolidayLeave.Data.Entities
{
    public abstract class AuditableEntity : BaseEntity, IAuditableEntity
    {
        public AuditableEntity()
        {
            InsertTime = DateTime.Now;
        }

        public DateTime InsertTime { get; set; }

        public DateTime? ChangeTime { get; set; }
    }
}
