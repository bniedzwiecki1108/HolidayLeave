﻿using System;

namespace HolidayLeave.Data.Entities
{
    public class HolidayLeaveEntity : AuditableEntity
    {
        public string Name { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }
    }
}
