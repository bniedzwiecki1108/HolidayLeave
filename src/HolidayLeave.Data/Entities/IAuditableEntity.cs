﻿using System;

namespace HolidayLeave.Data.Entities
{
    interface IAuditableEntity
    {
        DateTime InsertTime { get; set; }

        DateTime? ChangeTime { get; set; }
    }
}
