﻿using HolidayLeave.Core.Infrastructure.AutoMapper;
using HolidayLeave.Data.Entities;
using System;

namespace HolidayLeave.Data.Models
{
    public class HolidayLeaveModel
    {
        public string Name { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }
    }

    public class UserModelProfile : AutoMapperProfile
    {
        public UserModelProfile()
        {
            CreateMap<HolidayLeaveEntity, HolidayLeaveModel>();
        }
    }
}
