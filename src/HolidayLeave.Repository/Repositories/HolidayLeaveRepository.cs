﻿using System.Collections.Generic;
using HolidayLeave.Data;
using HolidayLeave.Data.Entities;

namespace HolidayLeave.Repository.Repositories
{
    public interface IHolidayLeaveRepository
    {
        void Add(HolidayLeaveEntity holidayLeaveCreateModel);

        List<HolidayLeaveEntity> GetHolidayLeaves();

        void Clear();
    }

    public class HolidayLeaveRepository : IHolidayLeaveRepository
    {
        private readonly IData _data;

        public HolidayLeaveRepository(IData data)
        {
            _data = data;
        }

        public void Add(HolidayLeaveEntity holidayLeaveCreateModel) =>
            _data.Add(holidayLeaveCreateModel);

        public List<HolidayLeaveEntity> GetHolidayLeaves() =>
            _data.GetHolidayLeaves();

        public void Clear() =>
            _data.Clear();
    }
}
