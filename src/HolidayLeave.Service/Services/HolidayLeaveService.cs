﻿using AutoMapper;
using HolidayLeave.Data;
using HolidayLeave.Data.Entities;
using HolidayLeave.Data.Models;
using HolidayLeave.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HolidayLeave.Service.Services
{
    public interface IHolidayLeaveService
    {
        void Add(HolidayLeaveModel holidayLeaveModel);

        List<HolidayLeaveModel> GetHolidayLeaves();

        List<HolidayLeaveModel> GetHolidayLeavesBy(DateTime? date);

        void Clear();
    }

    public class HolidayLeaveService : IHolidayLeaveService
    {
        private readonly IHolidayLeaveRepository _holidayLeaveRepository;
        private readonly IData _file;

        public HolidayLeaveService(
            IHolidayLeaveRepository holidayLeaveRepository,
            IData file)
        {
            _file = file;
            _holidayLeaveRepository = holidayLeaveRepository;
        }

        public void Add(HolidayLeaveModel holidayLeaveModel) =>
            _holidayLeaveRepository.Add(
                new HolidayLeaveEntity
                {
                    Name = holidayLeaveModel.Name,
                    DateFrom = holidayLeaveModel.DateFrom,
                    DateTo = holidayLeaveModel.DateTo
                });

        public List<HolidayLeaveModel> GetHolidayLeaves() =>
              Mapper.Map<List<HolidayLeaveModel>>(
                  _holidayLeaveRepository.GetHolidayLeaves());

        public List<HolidayLeaveModel> GetHolidayLeavesBy(DateTime? date) =>
            GetHolidayLeaves()
                .Where(d =>
                    (d.DateFrom <= date)
                    && (d.DateTo >= date))
                .ToList();

        public void Clear() =>
            _holidayLeaveRepository
                .Clear();
    }
}
