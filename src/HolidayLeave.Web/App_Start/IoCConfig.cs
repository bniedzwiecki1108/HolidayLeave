﻿using Autofac;
using Autofac.Integration.Mvc;
using HolidayLeave.Data;
using HolidayLeave.Web.Infrastructure.Modules;
using System.Web.Mvc;

namespace HolidayLeave.Web.App_Start
{
    public class IoCConfig
    {
        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder
                .RegisterControllers(typeof(MvcApplication).Assembly)
                .PropertiesAutowired();

            builder
                .RegisterModule(new RepositoryModule());

            builder
                .RegisterModule(new ServiceModule());

            builder
                .RegisterModule(new AutoMapperModule());

            builder
               .RegisterType<Data.Data>()
               .As<IData>()
               .SingleInstance();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            return container;

        }
    }
}