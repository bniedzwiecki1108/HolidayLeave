﻿using System.Web.Mvc;

namespace HolidayLeave.Web.Controllers
{
    public class BaseController : Controller
    {
        protected void SetTemporaryMessage(string temporaryMessage, string temporaryType)
        {
            TempData["TemporaryMessage"] = temporaryMessage;
            TempData["TemporaryType"] = temporaryType;
        }

        public static class TemporaryType
        {
            public static string Danger = "alert alert-danger";
            public static string Succes = "alert alert-success";
            public static string Warning = "alert alert-warning";
        }
    }
}