﻿using AutoMapper;
using HolidayLeave.Data.Models;
using HolidayLeave.Service.Services;
using HolidayLeave.Web.ViewModels.HolidayLeave;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HolidayLeave.Web.Controllers
{
    public class HolidayLeaveController : BaseController
    {
        private const string SuccesAddHolidayLeave = "Dodano urlop";
        private const string RemoveHolidayLeave = "Urolpy zostały usunięte";

        private readonly IHolidayLeaveService _holidayLeaveService;

        public HolidayLeaveController(IHolidayLeaveService holidayLeaveService)
        {
            _holidayLeaveService = holidayLeaveService;
        }

        [HttpGet]
        public ActionResult Create() =>
            View(new HolidayLeaveCreateViewModel());

        [HttpPost]
        public ActionResult Create(HolidayLeaveCreateViewModel holidayLeaveCreateModel)
        {
            if (ModelState.IsValid)
            {
                var holidayLeaveModel = Mapper.Map<HolidayLeaveModel>(holidayLeaveCreateModel);
                _holidayLeaveService.Add(holidayLeaveModel);
                SetTemporaryMessage(SuccesAddHolidayLeave, TemporaryType.Succes);

                return RedirectToAction("Create");
            }

            return View(holidayLeaveCreateModel);
        }

        [HttpGet]
        public ActionResult Details()
        {
            var data = new HolidayLeaveDetailsModel
            {
                HolidayLeaves =
                   Mapper.Map<List<HolidayLeaveRowViewModel>>(
                       _holidayLeaveService
                           .GetHolidayLeaves())
            };

            return View(data);
        }

        [HttpPost]
        public ActionResult Details(HolidayLeaveDetailsModel holidayLeaveDetailsModel)
        {
            if (ModelState.IsValid)
            {
                var data = new HolidayLeaveDetailsModel
                {
                    HolidayLeaves =
                        Mapper.Map<List<HolidayLeaveRowViewModel>>(
                           _holidayLeaveService
                               .GetHolidayLeavesBy(holidayLeaveDetailsModel.Date))
                };
                return View(data);
            }
            return View(holidayLeaveDetailsModel);
        }

        [HttpGet]
        public ActionResult RemoveAllHolidayLeave()
        {
            _holidayLeaveService
                .Clear();

            SetTemporaryMessage(RemoveHolidayLeave, TemporaryType.Succes);

            return RedirectToAction("Details");
        }
    }
}