﻿using Autofac;

namespace HolidayLeave.Web.Infrastructure.Modules
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder) =>
            builder.RegisterAssemblyTypes(System.Reflection.Assembly.Load("HolidayLeave.Repository"))
                .Where(t => t.Name.EndsWith("Repository"))
                .AsSelf()
                .AsImplementedInterfaces();
    }
}