﻿using Autofac;
using System.Linq;

namespace HolidayLeave.Web.Infrastructure.Modules
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder) =>
            builder.RegisterAssemblyTypes(System.Reflection.Assembly.Load("HolidayLeave.Service"))
                .Where(t => t.Name.EndsWith("Service"))
                .AsSelf()
                .AsImplementedInterfaces();
    }
}