﻿$(function () {
    $('.date-picker').datepicker({        
        dateFormat: "yy.mm.dd"        
    });
});

$(".input-group-addon").on("click", function () {
    $(this).siblings("input").datepicker("show");
});