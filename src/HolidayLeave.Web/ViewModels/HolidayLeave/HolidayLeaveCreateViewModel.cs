﻿using HolidayLeave.Core.Infrastructure.AutoMapper;
using HolidayLeave.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HolidayLeave.Web.ViewModels.HolidayLeave
{
    public class HolidayLeaveCreateViewModel : IValidatableObject
    {
        [Required(ErrorMessage = "Pole nie może być puste")]
        [Display(Name = "Imię")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Pole nie może być puste")]
        [Display(Name = "Data od")]
        public DateTime? DateFrom { get; set; }

        [Required(ErrorMessage = "Pole nie może być puste")]
        [Display(Name = "Data do")]
        public DateTime? DateTo { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (DateFrom < DateTime.Now)
            {
                yield return new ValidationResult(
                    errorMessage: "Pole ‘Data od’ może zawierać tylko daty z przyszłości",
                    memberNames: new[] { "DateFrom" });
            }

            if (DateTo < DateFrom)
            {
                yield return new ValidationResult(
                    errorMessage: "Pole ‘Data do’ może zawierać tylko daty późniejsze od ‘Data od’",
                    memberNames: new[] { "DateTo" });
            }
        }
    }

    public class HolidayLeaveCreateViewModelProfile : AutoMapperProfile
    {
        public HolidayLeaveCreateViewModelProfile()
        {
            CreateMap<HolidayLeaveModel, HolidayLeaveCreateViewModel>();
        }
    }    
}