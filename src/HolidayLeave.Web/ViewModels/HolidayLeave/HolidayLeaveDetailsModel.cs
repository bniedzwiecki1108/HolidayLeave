﻿using HolidayLeave.Core.Infrastructure.AutoMapper;
using HolidayLeave.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HolidayLeave.Web.ViewModels.HolidayLeave
{

    public class HolidayLeaveDetailsModel
    {
        public List<HolidayLeaveRowViewModel> HolidayLeaves { get; set; }

        [Display(Name = "Data")]
        public DateTime? Date { get; set; }
    }

    public class HolidayLeaveRowViewModel
    {
        public string Name { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }     
    }

    public class HolidayLeaveDetailsModelProfile : AutoMapperProfile
    {
        public HolidayLeaveDetailsModelProfile()
        {
            CreateMap<HolidayLeaveModel, HolidayLeaveRowViewModel>();
        }
    }
}